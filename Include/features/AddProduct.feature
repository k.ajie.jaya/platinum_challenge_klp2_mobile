#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@AddProduct
Feature: Add Product to sell
  User want to add product to sell with valid format

  @TC-MOB-PROD-004.002
  Scenario: Add product to sell with valid format
    Given User launch mobile apps secondhand
    When User login
    And User tap button tambah produk
    And User input new product name
    And User input new product price
    And User input new product location
    And User input new product description
    And User tap add new photo product
    And User tap camera button
    And User tap capture button
    And User tap button finish
    Then User tap button Terbitkan
    