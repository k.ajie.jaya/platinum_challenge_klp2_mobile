#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@EditProduct
Feature: Edit Product Data
  User want to edit product that they already posted

  @TC-MOB-PROD-004.003
  Scenario: Edit product from product list in Daftrar Jual page
    Given User launch mobile apps secondhand
    When User login
    And User tap navlink Akun
    And User tap daftar jual saya
    And User tap product that they want to edit
    And User tap field Nama Produk
    And User tap button X
    And User input new Nama Produk
    And User tap field Harga Produk
    And User tap button X
    And User input new Harga Produk
    And User tap field Lokasi
    And User tap button X
    And User input new Lokasi
    And User scroll to text Foto Produk
    And User tap field Deskripsi
    And User tap button X
    And User input new Deskripsi
    Then User tap button Perbarui Produk 
    
    