#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@BuyProduct
Feature: Buy Product
  User want to buy product

  @TS-MOB-BUY-005
  Scenario: Buy product from product list in home page
    Given User launch mobile apps secondhand
    When User login
    And User tap navlink Home
    And User tap product
    And User tap button "Saya Tertarik dan Ingin Nego"
    And User input negotiation price
    And User tap kirim button
