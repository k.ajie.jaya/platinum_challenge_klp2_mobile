#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Login
Feature: Login
  User want to login in secondhand mobile apps
  
   Scenario: Login using invalid credential
    Given User launch mobile apps secondhand
    When User tap Akun image
    And User tap button masuk
    And User input invalid email
    And User input invalid password
    And User tap button login
    And User delete email field
    And User delete password field
  

  Scenario: Login using valid credential
    And User input valid email
    And User input valid password
    And User tap button login
