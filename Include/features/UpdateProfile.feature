#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@UpdateProfile
Feature: Update Profile
  User want to update profile

  @TC-MOB-PROF-003.001
  Scenario: Update profile using invalid format
    Given User launch mobile apps secondhand
    When User login
    And User tap pencil icon
    And User tap profile name
    And User delete profile data
    And User input invalid format profile name
    And User tap simpan button
   
   @TC-MOB-PROF-003.002 
   Scenario: Update profile using valid format
    When User tap profile name
    And User delete profile data
    And User input valid format profile name
    And User tap simpan button
    And User tap profile phone number
    And User delete profile data
    And User input valid format phone number
    And User tap simpan button
    And User tap profile city
    And User delete profile data
    And User input valid format profile city
    And User tap simpan button
    And User tap profile address
    And User delete profile data
    And User input valid format profile address
    And User tap simpan button