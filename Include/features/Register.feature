#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Register
Feature: Register
  User want to register in secondhand mobile apps

@TC-MOB-REG-001.002
  Scenario: Register using invalid data
    Given User launch mobile apps secondhand
    When User tap Akun image
    And User tap button masuk
    And User tap text Daftar
    And User input valid name
    And User input invalid register email
    And User input valid register password
    And User input valid phone number
    And User input valid city
    And User input valid address
    Then User tap button daftar

@TC-MOB-REG-001.001
	Scenario: Register using valid data
		When User delete register email field
		And User input valid register email
		Then User tap button daftar
