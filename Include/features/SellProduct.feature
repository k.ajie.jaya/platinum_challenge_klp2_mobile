#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@SellProduct
Feature: Sell Product
  User want to sell product

  @TC-MOB-SELL-006.001
  Scenario: Update accept product offer in diminati page
    Given User launch mobile apps secondhand
    When User login
    And User tap navlink Akun
    And User tap daftar jual saya
    And User tap navlink Diminati
    And User tap product offer that meets seller requirement
    And User tap button Terima
    And User scroll down
    And User tap button back
    
  @TC-MOB-SELL-006.002
  Scenario: Update reject product offer in diminati page
  	When User tap product offer that didn'nt meets seller requirement
  	And User tap button Tolak
    
    
    
    
