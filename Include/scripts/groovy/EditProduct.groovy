import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class EditProduct {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("User tap product that they want to edit")
	public void user_tap_product_that_they_want_to_edit() {
		Mobile.tap(findTestObject('Daftar Jual Saya/Produk1'), 0)
	}

	@When("User tap field Nama Produk")
	public void user_tap_field_Nama_Produk() {
		Mobile.tap(findTestObject('Ubah Produk/field - Nama Produk'), 0)
	}

	@When("User tap button X")
	public void user_tap_button_X() {
		Mobile.tap(findTestObject('Ubah Produk/button-X'), 0)
	}

	@When("User input new Nama Produk")
	public void user_input_new_Nama_Produk() {
		Mobile.setText(findTestObject('Ubah Produk/Field - Nama Produk'), 'iMac (M1)', 0)
	}

	@When("User tap field Harga Produk")
	public void user_tap_field_Harga_Produk() {
		Mobile.tap(findTestObject('Ubah Produk/field - Harga Produk'), 0)
	}

	@When("User input new Harga Produk")
	public void user_input_new_Harga_Produk() {
		Mobile.setText(findTestObject('Ubah Produk/field - Harga Produk'), '15000000', 0)
	}

	@When("User tap field Lokasi")
	public void user_tap_field_Lokasi() {
		Mobile.tap(findTestObject('Ubah Produk/field - Lokasi'), 0)
	}

	@When("User input new Lokasi")
	public void user_input_new_Lokasi() {
		Mobile.setText(findTestObject('Ubah Produk/field - Lokasi'), 'Jakarta', 0)
	}

	@When("User scroll to text Foto Produk")
	public void user_scroll_to_text_Foto_Produk() {
		Mobile.scrollToText('Foto Produk', FailureHandling.STOP_ON_FAILURE)
	}

	@When("User tap field Deskripsi")
	public void user_tap_field_Deskripsi() {
    	Mobile.tap(findTestObject('Ubah Produk/field - Deskripsi'), 0)
	}

	@When("User input new Deskripsi")
	public void user_input_new_Deskripsi() {
		Mobile.setText(findTestObject('Ubah Produk/field - Deskripsi'), 'iMac 24 inci (M1, Empat Port, 2021)', 0)
	}

	@Then("User tap button Perbarui Produk")
	public void user_tap_button_Perbarui_Produk() {
		Mobile.tap(findTestObject('Ubah Produk/button-Perbarui Produk'), 0)
	}
}