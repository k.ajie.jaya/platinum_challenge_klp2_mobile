import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class UpdateProfile {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("User tap pencil icon")
	public void user_tap_pencil_icon() {
		Mobile.tap(findTestObject('Akun Saya/AkunSaya_Update Data'), 0)
	}
	
	@When("User tap profile name")
	public void user_tap_profile_name() {
		Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun_Edit Nama'), 0)
	}
	
	@When("User delete profile data")
	public void user_delete_profile_data() {
		Mobile.clearText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 0)
	}
	
	@When("User input invalid format profile name")
	public void user_input_invalid_format_profile_name() {
		Mobile.setText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), '/.?', 0)
	}
	
	@When("User tap simpan button")
	public void user_tap_simpan_button() {
		Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Button Simpan'), 0)
	}
	
	
	@When("User input valid format profile name")
	public void user_input_valid_format_profile_name() {
		Mobile.setText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 'kinofeje semangat', 0)
	}
	
	@When("User tap profile phone number")
	public void user_tap_profile_phone_number() {
		Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Nomor HP'), 0)
	}
	
	@When("User input valid format phone number")
	public void user_input_valid_format_phone_number() {
		Mobile.setText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), '082257042215', 0)
	}
	
	@When("User tap profile city")
	public void user_tap_profile_city() {
		Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Kota'), 0)
	}
	
	@When("User input valid format profile city")
	public void user_input_valid_format_profile_city() {
		Mobile.setText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 'Bandung', 0)
	}
	
	@When("User tap profile address")
	public void user_tap_profile_address() {
		Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Alamat'), 0)
	}
	
	@Given("User input valid format profile address")
	public void user_input_valid_format_profile_address() {
		Mobile.setText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 'Bandung Ramai', 0)
	}
	
}