import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Login {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@When("User input invalid email")
	public void user_input_invalid_email() {
		Mobile.setText(findTestObject('Login/Login_Masukkan email'), 'wkwk12', 0)
	}

	@When("User input invalid password")
	public void user_input_invalid_password() {
		Mobile.setText(findTestObject('Login/Login_Masukkan password'), '12345678', 0)
	}


	@When("User delete email field")
	public void user_delete_email_field() {
		Mobile.clearText(findTestObject('Login/Login_Masukkan email'), 0)
	}

	@When("User delete password field")
	public void user_delete_password_field() {
		Mobile.clearText(findTestObject('Login/Login_Masukkan password'), 0)
	}

	@When("User input valid email")
	public void user_input_valid_email() {
		Mobile.setText(findTestObject('Login/Login_Masukkan email'), GlobalVariable.username, 0)
	}

	@When("User input valid password")
	public void user_input_valid_password() {
		Mobile.setText(findTestObject('Login/Login_Masukkan password'), GlobalVariable.password, 0)
	}

	@When("User tap button login")
	public void user_tap_button_login() {
		Mobile.tap(findTestObject('Login/Login_Button-Masuk'), 0)
	}
}










