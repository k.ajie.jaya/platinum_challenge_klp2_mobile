import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class AddProduct {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("User tap button tambah produk")
	public void user_tap_button_tambah_produk() {
		Mobile.tap(findTestObject('Tambah Produk/TambahProduk_Button - Terbitkan'), 0)
	{	

	@When("User input new product name")
	public void user_input_new_product_name() {
		Mobile.setText(findTestObject('Tambah Produk/EditText - Nama Produk'), 'Test1', 0)
	}

	@When("User input new product price")
	public void user_input_new_product_price() {
		Mobile.setText(findTestObject('Tambah Produk/TambahProduk_EditText - Harga Produk'), '1000', 0)
	}

	@When("User input new product location")
	public void user_input_new_product_location() {
		Mobile.setText(findTestObject('Tambah Produk/TambahProduk_EditText - Lokasi Produk'), 'bandung', 0)
	}

	@When("User input new product description")
	public void user_input_new_product_description() {
		Mobile.setText(findTestObject('Tambah Produk/TambahProduk_EditText - Deskripsi'), 'bagus', 0)
	}

	@When("User tap add new photo product")
	public void user_tap_add_new_photo_product() {
		Mobile.tap(findTestObject('Tambah Produk/TambahProduk_ImageView - Foto Produk'), 0)
	}

	@When("User tap camera button")
	public void user_tap_camera_button() {
		Mobile.tap(findTestObject('Tambah Produk/Button - take photo'), 0)
	}

	@When("User tap capture button")
	public void user_tap_capture_button() {
		Mobile.tap(findTestObject('Tambah Produk/Button - Capture'), 0)
	}

	@When("User button finish")
	public void user_button_finish() {
		Mobile.tap(findTestObject('Tambah Produk/Button - Ceklis'), 0)
	}

	@Then("User tap button Terbitkan")
	public void user_tap_button_Terbitkan() {
    Mobile.tap(findTestObject('Tambah Produk/TambahProduk_Button - Terbitkan'), 0)
	}
}