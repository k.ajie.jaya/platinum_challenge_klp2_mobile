import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Common {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("User launch mobile apps secondhand")
	public void user_launch_mobile_apps_secondhand() {
		Mobile.startApplication(GlobalVariable.Apps, false)
	}

	@When("User tap Akun image")
	public void user_tap_Akun_image() {
		Mobile.tap(findTestObject('Akun/Akun_ImageView'), 0)
	}

	@When("User tap button masuk")
	public void user_tap_button_masuk() {
		Mobile.tap(findTestObject('Akun/Akun_Button-Masuk'), 0)
	}

	@When("User login")
	public void user_login() {
		Mobile.tap(findTestObject('Akun/Akun_ImageView'), 0)
		Mobile.tap(findTestObject('Akun/Akun_Button-Masuk'), 0)
		Mobile.setText(findTestObject('Login/Login_Masukkan email'), GlobalVariable.username, 0)
		Mobile.setText(findTestObject('Login/Login_Masukkan password'), GlobalVariable.password, 0)
		Mobile.tap(findTestObject('Login/Login_Button-Masuk'), 0)
	}
}