import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class BuyProduct {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@When("User tap navlink Home")
	public void user_tap_navlink_Home() {
		Mobile.tap(findTestObject('Home/NavLink-Beranda'), 0)
	}

	@When("User tap product")
	public void user_tap_product() {
		Mobile.tap(findTestObject('Home/Produk - 1'), 0)
	}

	@When("User tap button {string}")
	public void user_tap_button(String string) {
		Mobile.tap(findTestObject('Beli Product/Button - Saya Tertarik dan Ingin Nego'), 0)
	}

	@When("User input negotiation price")
	public void user_input_negotiation_price() {
		Mobile.setText(findTestObject('Beli Product/EditText - Rp 0,00'), '2000', 0)
	}

	@When("User tap kirim button")
	public void user_tap_kirim_button() {
		Mobile.tap(findTestObject('Beli Product/Button-Kirim'), 0)
	}
}