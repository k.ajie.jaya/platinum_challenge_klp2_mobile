import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class SellProduct {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@When ("User tap navlink Diminati")
	public void user_tap_navlink_diminati() {
		Mobile.tap(findTestObject('Daftar Jual Saya/NavLink - Diminati'), 0)
	}

	@When("User tap product offer that meets seller requirement")
	public void user_tap_product_offer_that_meets_seller_requirement() {
		Mobile.tap(findTestObject('Daftar Jual Saya/Produk - Diminati2'), 0)
	}

	@When("User tap button Terima")
	public void user_tap_button_Terima() {
		Mobile.tap(findTestObject('Daftar Jual Saya/Button - Terima'), 0)
	}

	@When("User scroll down")
	public void user_scroll_down() {
		Mobile.tap(findTestObject('Daftar Jual Saya/Page - Outside'), 0)
	}

	@When("User tap button back")
	public void user_tap_button_back() {
		Mobile.tap(findTestObject('Daftar Jual Saya/Button - Back'), 0)
	}

	@When("User tap product offer that didn'nt meets seller requirement")
	public void user_tap_product_offer_that_didn_nt_meets_seller_requirement() {
		Mobile.tap(findTestObject('Daftar Jual Saya/Produk - Diminati3'), 0)
	}

	@When("User tap button Tolak")
	public void user_tap_button_Tolak() {
		Mobile.tap(findTestObject('Daftar Jual Saya/Button - Tolak'), 0)
	}
}