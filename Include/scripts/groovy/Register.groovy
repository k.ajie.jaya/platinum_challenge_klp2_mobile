import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Register {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@When("User tap text Daftar")
	public void user_tap_text_Daftar() {
		Mobile.tap(findTestObject('Daftar/button_daftar'), 0)
	}

	@When("User input valid name")
	public void user_input_valid_name() {
		Mobile.setText(findTestObject('Daftar/Daftar_Masukkan nama lengkap'), 'a', 0)
	}

	@When("User input invalid register email")
	public void user_input_invalid_register_email() {
		Mobile.setText(findTestObject('Daftar/Daftar_Masukkan email'), 'wkwk12', 0)
	}

	@When("User input valid register password")
	public void user_input_valid_register_password() {
		Mobile.setText(findTestObject('Daftar/Daftar_Masukkan password'), GlobalVariable.password, 0)
	}

	@When("User input valid phone number")
	public void user_input_valid_phone_number() {
		Mobile.setText(findTestObject('Daftar/Daftar_Masukkan nomor HP'), '081234567', 0)
	}

	@When("User input valid city")
	public void user_input_valid_city() {
		Mobile.setText(findTestObject('Daftar/Daftar_Masukkan kota'), 'jakarta', 0)
	}

	@When("User input valid address")
	public void user_input_valid_address() {
		Mobile.setText(findTestObject('Daftar/Daftar_Masukkan alamat'), 'jakbar', 0)
	}

	@Then("User tap button daftar")
	public void user_tap_button_daftar() {
		Mobile.tap(findTestObject('Daftar/Daftar_Button-Daftar'), 0)
	}

	@When("User delete register email field")
	public void user_delete_register_email_field() {
		Mobile.clearText(findTestObject('Daftar/Daftar_Masukkan email'), 0)
	}

	@When("User input valid register email")
	public void user_input_valid_register_email() {
		Date email = new Date()
		String emailRegister = email.format('yyyyMMddHHmmss')
		def email_code = ('aab' + emailRegister) + '@gmail.com'
		Mobile.setText(findTestObject('Daftar/Daftar_Masukkan email'), email_code, 0)
	}
}