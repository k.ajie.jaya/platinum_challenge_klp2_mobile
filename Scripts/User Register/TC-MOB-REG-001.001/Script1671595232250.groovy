import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.Apps, false)

Mobile.tap(findTestObject('Akun/Akun_ImageView'), 0)

Mobile.tap(findTestObject('Akun/Akun_Button-Masuk'), 0)

Mobile.tap(findTestObject('Daftar/button_daftar'), 0)

Mobile.setText(findTestObject('Daftar/Daftar_Masukkan nama lengkap'), 'a', 0)

Date email = new Date()

String emailRegister = email.format('yyyyMMddHHmmss')

def email_code = ('aab' + emailRegister) + '@gmail.com'

Mobile.setText(findTestObject('Daftar/Daftar_Masukkan email'), email_code, 0)

Mobile.setText(findTestObject('Daftar/Daftar_Masukkan password'), '123456', 0)

Mobile.setText(findTestObject('Daftar/Daftar_Masukkan nomor HP'), '081234567', 0)

Mobile.setText(findTestObject('Daftar/Daftar_Masukkan kota'), 'jakarta', 0)

Mobile.setText(findTestObject('Daftar/Daftar_Masukkan alamat'), 'jakbar', 0)

Mobile.tap(findTestObject('Daftar/Daftar_Button-Daftar'), 0)

Mobile.clearText(findTestObject('Daftar/Daftar_Masukkan email'), 0)

