import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.Apps, false)

Mobile.tap(findTestObject('Home/NavLink-Akun'), 0)

Mobile.tap(findTestObject('Akun Saya/AkunSaya_TextView - Daftar Jual Saya'), 0)

Mobile.tap(findTestObject('Daftar Jual Saya/Produk1'), 0)

Mobile.tap(findTestObject('Ubah Produk/field - Nama Produk'), 0)

Mobile.tap(findTestObject('Ubah Produk/button-X'), 0)

Mobile.setText(findTestObject('Ubah Produk/Field - Nama Produk'), 'iMac (M1)', 0)

Mobile.tap(findTestObject('Ubah Produk/field - Harga Produk'), 0)

Mobile.tap(findTestObject('Ubah Produk/button-X'), 0)

Mobile.setText(findTestObject('Ubah Produk/field - Harga Produk'), '15000000', 0)

Mobile.tap(findTestObject('Ubah Produk/field - Lokasi'), 0)

Mobile.tap(findTestObject('Ubah Produk/button-X'), 0)

Mobile.setText(findTestObject('Ubah Produk/field - Lokasi'), 'Jakarta', 0)

Mobile.scrollToText('Foto Produk', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Ubah Produk/field - Deskripsi'), 0)

Mobile.tap(findTestObject('Ubah Produk/button-X'), 0)

Mobile.setText(findTestObject('Ubah Produk/field - Deskripsi'), 'iMac 24 inci (M1, Empat Port, 2021)', 0)

Mobile.tap(findTestObject('Ubah Produk/button-Perbarui Produk'), 0)

