import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.Apps, false)

Mobile.tap(findTestObject('Home/Button-Tambah Produk'), 0)

Mobile.setText(findTestObject('Tambah Produk/EditText - Nama Produk'), 'Test1', 0)

Mobile.setText(findTestObject('Tambah Produk/TambahProduk_EditText - Harga Produk'), '1000', 0)

Mobile.setText(findTestObject('Tambah Produk/TambahProduk_EditText - Lokasi Produk'), 'bandung', 0)

Mobile.setText(findTestObject('Tambah Produk/TambahProduk_EditText - Deskripsi'), 'bagus', 0)

Mobile.tap(findTestObject('Tambah Produk/TambahProduk_ImageView - Foto Produk'), 0)

Mobile.tap(findTestObject('Tambah Produk/Button - take photo'), 0)

Mobile.tap(findTestObject('Tambah Produk/Button - Capture'), 0)

Mobile.tap(findTestObject('Tambah Produk/Button - Ceklis'), 0)

Mobile.tap(findTestObject('Tambah Produk/TambahProduk_Button - Terbitkan'), 0)

