import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.tap(findTestObject('Akun Saya/AkunSaya_Update Data'), 0)

Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun_Edit Nama'), 0)

Mobile.clearText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 0)

Mobile.setText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 'kinofeje semangat', 0)

Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Button Simpan'), 0)

Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Nomor HP'), 0)

Mobile.clearText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 0)

Mobile.setText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), '081234567', 0)

Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Button Simpan'), 0)

Mobile.verifyElementVisible(findTestObject('Lengkapi Info Akun/Pop up message-Profil berhasil diperbarui'), 0)

Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Kota'), 0)

Mobile.clearText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 0)

Mobile.setText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 'Bandung', 0)

Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Button Simpan'), 0)

Mobile.verifyElementVisible(findTestObject('Lengkapi Info Akun/Pop up message-Profil berhasil diperbarui'), 0)

Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Alamat'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.clearText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 0)

Mobile.setText(findTestObject('Lengkapi Info Akun/InfoAkun-Edit Text'), 'Bandung Timur', 0)

Mobile.tap(findTestObject('Lengkapi Info Akun/InfoAkun-Button Simpan'), 0)

Mobile.verifyElementVisible(findTestObject('Lengkapi Info Akun/Pop up message-Profil berhasil diperbarui'), 0)

